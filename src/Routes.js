import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./pages/Login.js";
import InclusaoAtivos from "./pages/InclusaoAtivos.js";
import Dashboard from "./pages/Dashboard.js";
import AtivoAltera from "./pages/AlteraAtivo.js";
import TotalAtivos from "./pages/Total_Ativos.js";
import CadastroUsers from "./pages/CadastroInfoUsers.js";
import Resultado from "./pages/Resultado_op";
import Home from "./pages/Home";
import Erro from "./pages/erro.js";

function Rotas() {
  const [token, setToken] = useState(localStorage.getItem("token"));

  useEffect(() => {
    const getTokenFromLocalStorage = () => {
      const storedToken = localStorage.getItem("token");
      setToken(storedToken);
    };

    getTokenFromLocalStorage();
  }, []);

  return (
    <Router>
      <Switch>
        <Route path="/" exact> <Login /> </Route>
        <Route path="/cadastro" exact component={CadastroUsers} />
        <Route path="/erro" exact component={Erro} />
        
        {token && (
          <>
            <Route path="/home" component={Home} />
            <Route path="/adicionar" exact component={InclusaoAtivos} />
            <Route path="/visualizar" exact component={Dashboard} />
            <Route path="/edita/:id" exact component={AtivoAltera} />
            <Route path="/visualiza_tudo" exact component={TotalAtivos} />
            <Route path="/resultado" exact component={Resultado} />
          </>
        )}
        <Route path="*" component={Erro} />
      </Switch>
    </Router>
  );
}

export default Rotas;
