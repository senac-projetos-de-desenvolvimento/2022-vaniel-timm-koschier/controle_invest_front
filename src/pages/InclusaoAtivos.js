import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import Conecta from "./config/Conecta.js";
import Menu from "./Menu";
import "./css/inclusao.css";

const InclusaoAtivos = () => {
  const { register, handleSubmit, reset, setValue } = useForm();
  const [ativos, setAtivos] = useState([]);
  const [aviso, setAviso] = useState("");

  useEffect(() => {
    const fetchAtivos = async () => {
      try {
        const response = await Conecta.get("ativos_somados");
        setAtivos(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchAtivos();
  }, []);

  const handleCodigoChange = (event) => {
    const codigoSelecionado = event.target.value;
    const ativoSelecionado = ativos.find((ativo) => ativo.codigo === codigoSelecionado);
    if (ativoSelecionado) {
      setValue("nome", ativoSelecionado.nome);
      setValue("setor", ativoSelecionado.setor);
    } else {
      setValue("nome", "");
      setValue("setor", "");
    }
  };

  const salvar = async (campos) => {
    try {
      const response = await Conecta.post("adiciona", campos);
      setAviso(`Ok! Produto cadastrado com código ${response.data.id}`);
    } catch (error) {
      setAviso(`Erro... Produto não cadastrado: ${error}`);
    }
    setTimeout(() => {
      setAviso("");
    }, 5000);
    reset({
      ordem: "",
      codigo: "",
      valor: "",
      quant: "",
      data_op: ""
    });
    console.log(campos);
  };

  return (
    <div className="forminclusao">
      <Menu />
      <div className="container">
        <div>
          <nav className="navbar navbar-dark bg-primary">
            <a className="navbar-brand">Controle Invest</a>
            <form className="form-inline">
              <i className="mr-4">Cadastro de Ativos</i>
            </form>
          </nav>
        </div>
        <form onSubmit={handleSubmit(salvar)}>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="inputState">Tipo de Ordem</label>
              <select id="ordem" className="form-control" required autoFocus {...register("ordem")}>
                <option value="">Selecione...</option>
                <option value="Ordem de Compra">Ordem de Compra</option>
                <option value="Ordem de Venda">Ordem de Venda</option>
              </select>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="codigo">Código da Ação</label>
              <input
                type="text"
                className="form-control"
                id="codigo"
                required
                autoFocus
                {...register("codigo")}
                onChange={handleCodigoChange}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="nome">Nome da Empresa</label>
              <input
                type="text"
                className="form-control"
                id="nome"
                placeholder="Empresa"
                required
                autoFocus
                {...register("nome")}
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="setor">Setores</label>
              <input
                type="text"
                className="form-control"
                id="setor"
                required
                autoFocus
                {...register("setor")}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="valor">Valor De Operação</label>
              <input
                type="number"
                step="0.01"
                className="form-control"
                id="valor"
                placeholder="Valor"
                required
                autoFocus
                {...register("valor")}
              />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="quantidade">Quantidade</label>
              <input
                type="number"
                className="form-control"
                id="quantidade"
                placeholder="Quantidade"
                required
                autoFocus
                {...register("quant")}
              />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="data_op">Data de Operação</label>
              <input
                type="date"
                className="form-control"
                id="data_op"
                required
                autoFocus
                {...register("data_op")}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-12">
              <button type="submit" className="btn btn-primary mr-2">
                Salvar
              </button>
              <button type="reset" className="btn btn-danger">
                Limpar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default InclusaoAtivos;
