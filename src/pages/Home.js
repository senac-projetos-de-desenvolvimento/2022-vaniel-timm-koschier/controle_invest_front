import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Chart } from 'react-google-charts';
import Menu from './Menu';
import './css/Dashboard.css';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const Home = () => {
  const [dataPorcentagemAtivos, setDataPorcentagemAtivos] = useState([]);
  const [dataPorcentagemSetor, setDataPorcentagemSetor] = useState([]);
  const [newsData, setNewsData] = useState([]);

  useEffect(() => {
    const fetchDataPorcentagemAtivos = async () => {
      try {
        const res = await axios.get('https://node-api-24dt.onrender.com/porcent_ativos');
        setDataPorcentagemAtivos(res.data);
      } catch (err) {
        console.error(err);
      }
    };

    const fetchDataPorcentagemSetor = async () => {
      try {
        const res = await axios.get('https://node-api-24dt.onrender.com/porcent_setor');
        setDataPorcentagemSetor(res.data);
      } catch (err) {
        console.error(err);
      }
    };

    const fetchNewsData = async () => {
      try {
        const res = await axios.get(
          'https://newsdata.io/api/1/news?apikey=pub_24938de88f17ea71ea790d61343e828a97714&q=bolsa-de-valores&language=pt'
        );
        setNewsData(res.data.results);
      } catch (err) {
        console.error(err);
      }
    };

    fetchDataPorcentagemAtivos();
    fetchDataPorcentagemSetor();
    fetchNewsData();
  }, []);

  const generateChartDataAtivos = () => {
    const sortedData = dataPorcentagemAtivos.sort((a, b) => b.valor_total - a.valor_total);
    const chartData = [['Ativo', 'Porcentagem']];
    sortedData.forEach((entry, index) => {
      if (entry.valor_total !== 0) {
        chartData.push([entry.nome, entry.percentage]);
      }
    });
    return chartData;
  };

  const generateChartDataSetor = () => {
    const chartData = [['Setor', 'Porcentagem']];
    dataPorcentagemSetor.forEach((entry, index) => {
      chartData.push([entry.setor, entry.percentage]);
    });
    return chartData;
  };

  const generateBarChartData = () => {
    const chartData = [['Ativo', 'Valor Total']];
    dataPorcentagemAtivos.forEach((entry) => {
      if (entry.valor_total !== 0) {
      chartData.push([entry.nome, entry.valor_total]);
      }
    });
    return chartData;
  };

  return (
    <div className="dash">
      <Menu />
      <div className="container">
        <nav className="navbar navbar-dark bg-primary">
          <a className="navbar-brand">Controle Invest</a>
          <form className="form-inline">
            <i className="mr-4">Home</i>
          </form>
        </nav>
        <div className="chart">
            <h3 className="chart-title">Valor Total em Ativos</h3>
            <Chart
              width="100%"
              height="300px"
              chartType="BarChart"
              loader={<div>Carregando gráfico...</div>}
              data={generateBarChartData()}
              options={{
                colors: COLORS,
                hAxis: {
                  title: 'Valor Total',
                },
                vAxis: {
                  title: 'Ativo',
                },
              }}
            />
          </div>
        <div className="charts-wrapper">
          <div className="chart">
            <h3 className="chart-title">Porcentagem de Ativos</h3>
            <Chart
              width="100%"
              height="300px"
              chartType="PieChart"
              loader={<div>Carregando gráfico...</div>}
              data={generateChartDataAtivos()}
              options={{
                colors: COLORS,
                pieSliceText: 'percentage',
                pieSliceTextStyle: {
                  fontSize: 12,
                },
              }}
            />
          </div>
 
          <div className="chart">
            <h3 className="chart-title">Porcentagem por Setor</h3>
            <Chart
              width="100%"
              height="300px"
              chartType="PieChart"
              loader={<div>Carregando gráfico...</div>}
              data={generateChartDataSetor()}
              options={{
                colors: COLORS,
                pieSliceText: 'percentage',
                pieSliceTextStyle: {
                  fontSize: 12,
                },
              }}
            />
          </div>
          <div className="chart-square">
          <div className="news-title-wrapper">
            <h3 className="news-title">Notícias</h3>
          </div>
          <ul className="news-list">
            {newsData.map((news) => (
              <li key={news.title}>
                <a href={news.link} target="_blank" rel="noopener noreferrer">
                  {news.source_id}: {news.title}
                </a>
              </li>
            ))}
          </ul>
        </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
