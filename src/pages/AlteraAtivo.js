import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import Menu from './Menu';
import Conecta from "./config/Conecta";
import { withRouter } from 'react-router-dom';

const initialState = {
  nome: '',
  ordem: '',
  codigo: '',
  setor: '',
  valor: '',
  quantidade: '',
  data_op: '',
};

const formatDate = (dateString) => {
  const date = new Date(dateString);
  const day = String(date.getDate()).padStart(2, '0');
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const year = date.getFullYear();
  return `${day}/${month}/${year}`;
};

const AtivoAltera = ({ history, match }) => {
  const { register, handleSubmit, reset } = useForm({ defaultValues: initialState });
  const [errorMessage, setErrorMessage] = useState('');
  const [setorOptions, setSetorOptions] = useState([]);
  const [isOrdemVenda, setIsOrdemVenda] = useState(false);

  const onSubmit = (data) => {
    axios
      .put(`https://node-api-24dt.onrender.com/edita/${Number(match.params.id)}`, data)
      .then(() => {
        history.push('/visualizar');
        reset(initialState);
      })
      .catch((error) => {
        setErrorMessage('Failed to update the data. Please try again.');
        console.log(error);
      });
  };

  const clearForm = (event) => {
    event.preventDefault();
    reset(initialState);
  };

  useEffect(() => {
    axios
      .get(`https://node-api-24dt.onrender.com/ativo/${Number(match.params.id)}`)
      .then((response) => {
        const existingData = response.data;
        reset(existingData);
        setIsOrdemVenda(existingData.ordem === 'Ordem de Venda');
      })
      .catch((error) => {
        setErrorMessage(`Failed to fetch the existing data. Error: ${error.message}`);
        console.log(error);
      });

    axios
      .get('https://node-api-24dt.onrender.com/setores')
      .then((response) => {
        const setorData = response.data;
        setSetorOptions(setorData);
      })
      .catch((error) => {
        console.log('Failed to fetch setor data:', error);
      });
  }, [match.params.id, reset]);

  return (
    <div className="forminclusao">
      <Menu />
      <div className="container">
        <nav className="navbar navbar-dark bg-primary">
          <a className="navbar-brand">Controle Invest</a>
          <form className="form-inline">
            <i className="mr-4">Altera Ordens</i>
          </form>
        </nav>
        {errorMessage && <div className="error-message">{errorMessage}</div>}
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="ordem">Tipo de Ordem</label>
              <input
                id="ordem"
                className="form-control"
                type="text"
                value={isOrdemVenda ? 'Ordem de Venda' : 'Ordem de Compra'}
                readOnly
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="nome">Nome da Empresa</label>
              <input className="form-control" id="nome" placeholder="Empresa" {...register('nome')} />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="codigo">Código da Ação ou FI</label>
              <input type="text" className="form-control" id="codigo" placeholder="Código" {...register('codigo')} />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="setor">Setores</label>
              <input type="text" className="form-control" id="setor" placeholder="Setor" {...register('setor')} />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="valor">Valor de Operação</label>
              <input
                type="number"
                className="form-control"
                id="valor"
                placeholder="Valor"
                {...register('valor')}
                disabled={isOrdemVenda}
              />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="quantidade">Quantidade</label>
              <input
                type="number"
                className="form-control"
                id="quantidade"
                placeholder="Quantidade"
                {...register('quantidade')}
                disabled={isOrdemVenda}
              />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="data_op">Data de Operação</label>
              <input
                type="text"
                className="form-control"
                id="data_op"
                placeholder="Data"
                {...register('data_op')}
              />
            </div>
          </div>
          <button type="submit" className="btn btn-primary mr-2">
            Salvar
          </button>
          <button onClick={clearForm} className="btn btn-danger">
            Limpar
          </button>
        </form>
      </div>
    </div>
  );
};

export default withRouter(AtivoAltera);
