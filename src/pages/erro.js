import React from 'react';
import { withRouter } from 'react-router-dom';
import './css/erro.css'; // Importe o arquivo CSS para estilização

const Erro = ({ history }) => {
  const handleLogin = () => {
    // Redirecionar para a tela de login
    history.push('/');
  };

  return (
    <div className="erro-container">
      <div className="logo-container">
        <img
          src="https://i.postimg.cc/TPSC6Y9h/logo-maior.png"
          width="350px"
          alt="Logo"
          className="logo"
        />
      </div>
      <h1>Por favor, realize o login!</h1>
      <button onClick={handleLogin}>Realizar Login</button>
    </div>
  );
};

export default withRouter(Erro);
