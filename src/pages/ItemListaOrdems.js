import './css/ItemLista.css'
const ItemLista = ({id, ordem, nome, codigo, setor, valor, quantidade, data_op, excluirClick, alterarClick}) => {
    return (
        <tr>
            <td>{ordem}</td>
            <td>{nome}</td>
            <td>{codigo}</td>
            <td>{setor}</td>
            <td class="text-end">
                {"US$ " + Number(valor).toLocaleString("en-US", { style: 'currency', currency: 'USD' }).slice(1)}
            </td>
            <td>{quantidade}</td>
            <td>{data_op}</td>
            <td style={{ display: 'flex', alignItems: 'center' }}>
                <button className="btn btn-danger btn-sm" onClick={excluirClick}>
                  Excluir
                </button>
                <span style={{ marginLeft: '10px' }}></span>
                <button className="btn btn-primary btn-sm" onClick={alterarClick}>
                  Editar
                </button>
            </td>
        </tr>
    );
};


export default ItemLista;
