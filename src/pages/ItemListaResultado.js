import './css/ItemLista.css'
const ItemLista = ({nome, codigo, setor, data_op, resultado, diferenca_percentual}) => {
    return (
        <tr>
            <td>{nome}</td>
            <td>{codigo}</td>
            <td>{setor}</td>
            <td>{'US$ ' + resultado}</td>
            <td>{diferenca_percentual + '%'}</td>
            <td>{data_op}</td>
        </tr>
    );
};
export default ItemLista;