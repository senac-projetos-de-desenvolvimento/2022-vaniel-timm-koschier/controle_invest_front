import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import Conecta from "./config/Conecta";
import ItemLista from "./ItemListaOrdems";
import Menu from "./Menu";
import "./css/Dashboard.css";
import NavBar from "./NavBar";
import jsPDF from "jspdf";
import "jspdf-autotable";

const Dashboard = () => {
  const history = useHistory();
  const [ativos, setAtivos] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(10);
  const [ordemType, setOrdemType] = useState('all');
  const [sortBy, setSortBy] = useState('asc');
  const { register, handleSubmit, reset } = useForm();

  const obterLista = async () => {
    try {
      const lista = await Conecta.get("ativos");
      setAtivos(lista.data);
    } catch (error) {
      alert(`Erro... Não foi possível obter os dados: ${error}`);
    }
  };

  useEffect(() => {
    obterLista();
  }, []);

  const excluir = async (id, codigo) => {
    if (!window.confirm(`Confirma a exclusão da ação ${codigo}?`)) {
      return;
    }
    try {
      await Conecta.delete(`exclui/${id}`);
      setAtivos(ativos.filter((ativos) => ativos.id !== id));
    } catch (error) {
      alert(`Erro... Não foi possível excluir este produto: ${error}`);
    }
  };

  const carrega = (id, nome) => {
    if (!window.confirm(`Confirma que quer alterar algum dado desse ativo ${nome}?`)) {
      return;
    }
    try {
      history.push(`/edita/${id}`);
    } catch (error) {
      alert(`Erro... Não foi possível editar este produto: ${error}`);
    }
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  };

  const handleGerarRelatorio = () => {
    const doc = new jsPDF();

    doc.autoTable({
      head: [
        ["Ordem", "Nome da Empresa", "Código do Ativo", "Setor", "Valor", "Quantidade", "Data de Operação"]
      ],
      body: ativos.map((ativo) => [
        ativo.ordem,
        ativo.nome,
        ativo.codigo,
        ativo.setor,
        ativo.valor,
        ativo.quantidade,
        ativo.data_op
      ])
    });

    doc.save("relatorio_ordems.pdf");
  };

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const sortItems = () => {
    const sortedItems = [...ativos];

    sortedItems.sort((a, b) => {
      const dateA = new Date(a.data_op.split('/').reverse().join('/'));
      const dateB = new Date(b.data_op.split('/').reverse().join('/'));

      if (sortBy === 'asc') {
        return dateA.getTime() - dateB.getTime();
      } else {
        return dateB.getTime() - dateA.getTime();
      }
    });

    setAtivos(sortedItems);
    setSortBy(sortBy === 'asc' ? 'desc' : 'asc');
  };

  const filteredAtivos = ordemType === 'all' ? ativos : ativos.filter(ativo => ativo.ordem === ordemType);
  const lastItemIndex = currentPage * itemsPerPage;
  const firstItemIndex = lastItemIndex - itemsPerPage;
  const currentItems = filteredAtivos.slice(firstItemIndex, lastItemIndex);
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(filteredAtivos.length / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div className="dash">
      <Menu />
      <div className="container">
        <div>
          <NavBar />
        </div>
        <table className="table table-striped mt-3">
          <thead>
            <tr>
              <th>Ordem</th>
              <th>Nome da Empresa</th>
              <th>Codigo do Ativo</th>
              <th>Setor</th>
              <th>Valor</th>
              <th>Quantidade</th>
              <th onClick={sortItems} className="sort-btn">
                 
                  Data de Operação {sortBy === 'asc' ? '▲' : '▼'}
              </th>
              <th>Opções</th>
            </tr>
          </thead>
          <tbody>
            {currentItems.map((ativo, index) => (
              <ItemLista
                key={index}
                ordem={ativo.ordem}
                nome={ativo.nome}
                codigo={ativo.codigo}
                setor={ativo.setor}
                valor={ativo.valor}
                quantidade={ativo.quantidade}
                data_op={ativo.data_op}
                excluirClick={() => excluir(ativo.id, ativo.codigo)}
                alterarClick={() => carrega(ativo.id, ativo.nome)}
              />
            ))}
          </tbody>
        </table>
        <div className="pagination-container">
          <nav>
            <ul className="pagination">
              {pageNumbers.map((number) => (
                <li key={number} className="page-item">
                  <a
                    onClick={(event) => {
                      event.preventDefault();
                      paginate(number);
                    }}
                    href="#!"
                    className="page-link"
                  >
                    {number}
                  </a>
                </li>
              ))}
            </ul>
          </nav>
        </div>
        <button className="gerar-relatorio-btn" onClick={handleGerarRelatorio}>
          Gerar Relatório
        </button>
        <div className="ordem-type-container">
          <button className="ordem-type-btn" onClick={() => setOrdemType('all')}>
            Todas
          </button>
          <button className="ordem-type-btn" onClick={() => setOrdemType('Ordem de Compra')}>
            Compra
          </button>
          <button className="ordem-type-btn" onClick={() => setOrdemType('Ordem de Venda')}>
            Venda
          </button>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
