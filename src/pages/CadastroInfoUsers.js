import { useState } from "react";
import { useForm } from "react-hook-form";
import Conecta from "./config/Conecta.js";
import { useHistory } from "react-router-dom";
import "./css/cadastro.css"

const CadastroUsers = () => {
  const { register, handleSubmit, reset } = useForm();
  const [aviso, setAviso] = useState("");
  let history = useHistory();

  const salvar = async (campos) => {
    try {
      const response = await Conecta.post("usuarios", campos);
      setAviso(`Ok! Informações cadastradas com Sucesso${response.data.id}`);
      history.push("/");
    } catch (error) {
      setAviso(`Erro... Informações não Cadastradas: ${error}`);
    }
    setTimeout(() => {
      setAviso("");
    }, 5000);
    reset({
      nome: "",
      cpf: "",
      email: "",
      data_nasc: "",
    });
  };

  return (
    <div className="container2">

        <div className="logo-container">
          <img
            src="https://i.postimg.cc/TPSC6Y9h/logo-maior.png"
            width="350px"
            alt="Logo"
            className="logo"
          />
        </div>
        <form onSubmit={handleSubmit(salvar)}>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="inputAddress">Nome Completo</label>
              <input
                type="text"
                className="form-control"
                id="inputAddress"
                placeholder="Nome"
                required
                autoFocus
                {...register("nome")}
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="inputAddress2">CPF</label>
              <input
                type="number"
                className="form-control"
                id="inputAddress2"
                placeholder="CPF"
                required
                autoFocus
                {...register("cpf")}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="inputEmail4">Email para Cadastro</label>
              <input
                type="email"
                className="form-control"
                id="inputEmail4"
                placeholder="Email"
                required
                autoFocus
                {...register("email")}
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="inputPassword4">Data de Nascimento</label>
              <input
                type="date"
                className="form-control"
                id="inputPassword4"
                required
                autoFocus
                {...register("data_nasc")}
              />
            </div>
          </div>
          <button type="submit" className="btn btn-primary mr-2">
                Salvar
              </button>
              <button type="reset" className="btn btn-danger">
                Limpar
              </button>
        </form>
      </div>

  );
};

export default CadastroUsers;
