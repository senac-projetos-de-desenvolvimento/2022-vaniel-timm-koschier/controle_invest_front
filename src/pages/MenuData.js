import React from "react" 
import HomeIcon from '@mui/icons-material/Home';
import FormatAlignJustifyIcon from '@mui/icons-material/FormatAlignJustify';
import DashboardIcon from '@mui/icons-material/Dashboard';
import AssignmentIcon from '@mui/icons-material/Assignment';
import PaidIcon from '@mui/icons-material/Paid';
export const MenuData = [
{
    title:"Home",
    icon: <HomeIcon />,
    link: "/home",
},
{
    title:"Cadastro de Ativos",
    icon: <FormatAlignJustifyIcon />,
    link: "/adicionar",
},
{
    title:"Ordems de Compra e Venda",
    icon: <AssignmentIcon />,
    link: "/visualizar",
},
{
    title:"Dashboard de Ativos",
    icon: <DashboardIcon />,
    link: "/visualiza_tudo",
},
{
    title:"Resultado de Operações",
    icon: <PaidIcon />,
    link: "/resultado",
}
]
