import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import Conecta from "./config/Conecta";
import ItemLista from "./ItemListaTotal";
import Menu from "./Menu";
import "./css/Dashboard.css";
import NavBar_total from "./NavBar";
import jsPDF from "jspdf";
import "jspdf-autotable";

const TotalAtivos = () => {
  const [ativos_somados, setAtivos] = useState([]);
  const { register, handleSubmit, reset } = useForm();

  const obterLista = async () => {
    try {
      const lista = await Conecta.get("ativos_somados");
      setAtivos(lista.data);
    } catch (error) {
      alert(`Erro... Não foi possível obter os dados: ${error}`);
    }
  };

  useEffect(() => {
    obterLista();
  }, []);

  const handleGerarRelatorio = () => {
    const doc = new jsPDF();
    doc.setFont("Helvetica", "bold");
    doc.setFontSize(12);

    const tableData = ativos_somados.map((ativo) => [
      ativo.nome,
      ativo.codigo,
      ativo.setor,
      `US$ ${ativo.valor_medio}`,
      ativo.total_ativos
    ]);

    doc.autoTable({
      head: [["Nome da Empresa", "Código do Ativo", "Setor", "Valor Médio", "Quantidade Total"]],
      body: tableData,
      startY: 20
    });

    doc.save("relatorio_total_ativos.pdf");
  };

  return (
    <div className="dash">
      <Menu />
      <div className="dashboard-container container">
        <nav className="navbar navbar-dark bg-primary">
          <a className="navbar-brand">Controle Invest</a>
          <form className="form-inline">
            <i className="mr-4">Dashboard de Ativos</i>
          </form>
        </nav>
        <table className="table table-striped mt-3">
          <thead>
            <tr>
              <th>Nome da Empresa</th>
              <th>Codigo do Ativo</th>
              <th>Setor</th>
              <th>Valor Medio</th>
              <th>Quantidade Total</th>
              <th>Valor Atual</th>
              <th>Movimentação Total</th>
            </tr>
          </thead>
          <tbody>
            {ativos_somados.map((ativo_somado, index) => (
              <ItemLista
                key={index}
                nome={ativo_somado.nome}
                codigo={ativo_somado.codigo}
                setor={ativo_somado.setor}
                quantidade={ativo_somado.total_ativos}
                valor_medio={ativo_somado.valor_medio}
              />
            ))}
          </tbody>
        </table>
        <button className="gerar-relatorio-btn" onClick={handleGerarRelatorio}>
          Gerar Relatório
        </button>
      </div>
    </div>
  );
};

export default TotalAtivos;

