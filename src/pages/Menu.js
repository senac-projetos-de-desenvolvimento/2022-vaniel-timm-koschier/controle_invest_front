import React, { useContext } from "react";
import "../App.css";
import { ClienteContext } from "./config/ClienteContext";
import "./css/Menu.css";
import { MenuData } from "./MenuData";
import LogoutIcon from "@mui/icons-material/Logout";
import { useHistory } from "react-router-dom";

function Menu() {
  const cliente = useContext(ClienteContext);
  const history = useHistory();

  const logout = () => {
    // Remover o token e outras informações do cliente
    cliente.setDados({ id: null, nome: null, token: null });

    // Remover o token do localStorage
    localStorage.removeItem("token");

    // Redirecionar para a tela de login
    history.push("/");
  };

  return (
    <div className="tudo">
      <div className="total">
      <div className="sidebar">
  <ul className="sidebarlist">
    <div className="logo-container">
      <img
        src="https://i.postimg.cc/TPSC6Y9h/logo-maior.png"
        width={200}
        alt="Logo"
        className="logo"
      />
    </div>
            {MenuData.map((val, key) => {
              return (
                <li
                  key={key}
                  className={`coluna ${
                    window.location.pathname === val.link ? "active" : ""
                  }`}
                  onClick={() => {
                    window.location.pathname = val.link;
                  }}
                >
                  <div className="icon-2">{val.icon}</div>
                  <div className="title-2">{val.title}</div>
                </li>
              );
            })}
          </ul>
          <li className="coluna logout-button" onClick={logout}>
            <div className="icon-and-title">
              <LogoutIcon />
              <span>Logout</span>
            </div>
          </li>
        </div>
      </div>
    </div>
  );
}

export default Menu;
