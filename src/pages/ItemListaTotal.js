import { useState, useEffect } from 'react';
import './css/ItemLista.css'
import axios from 'axios';

const ItemLista = ({nome, codigo, setor, valor_medio, quantidade, excluirClick, alterarClick}) => {
    const [valorAtual, setValorAtual] = useState(0);
    const [diffPercent, setDiffPercent] = useState(0);

    useEffect(() => {
        const url = `https://finnhub.io/api/v1/quote?symbol=${codigo}&token=che2ms1r01qi6ghjv5j0che2ms1r01qi6ghjv5jg`;

        const fetchData = async () => {
            try {
                const response = await axios.get(url);
                setValorAtual(response.data.c);
                const diff = ((response.data.c - valor_medio) / valor_medio) * 100;
                setDiffPercent(diff);
            } catch (error) {
                console.error("Erro ao buscar dados da API do Finnhub: ", error);
            }
        };

        fetchData();

        const intervalId = setInterval(fetchData, 60000);

        return () => clearInterval(intervalId);
    }, [codigo, valor_medio]);

    return (
        <tr>
            <td>{nome}</td>
            <td>{codigo}</td>
            <td>{setor}</td>
            <td class="text-end">
                {"US$ " + Number(valor_medio).toLocaleString("en-US", { style: 'currency', currency: 'USD' }).slice(1)}
            </td>
            <td>{quantidade}</td>
            <td class="text-end">
                {"US$ " + Number(valorAtual).toLocaleString("en-US", { style: 'currency', currency: 'USD' }).slice(1)}
            </td>
            <td class="text-end">{diffPercent.toFixed(2)}%
            </td>
        </tr>
    );
};

export default ItemLista;
