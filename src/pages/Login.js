import React, { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { useHistory, Link } from "react-router-dom";
import Conecta from "./config/Conecta";
import { ClienteContext } from "./config/ClienteContext";
import "../App.css";

const Login = () => {
  const { register, handleSubmit } = useForm();
  const history = useHistory();
  const { setDados } = useContext(ClienteContext);
  const [errorMessage, setErrorMessage] = useState("");

  const onSubmit = async (data) => {
    try {
      const response = await Conecta.post("login", data);
      const { id, nome, token } = response.data;

      if (token) {
        setDados({ id, nome, token });
        localStorage.setItem("token", token);

        setTimeout(() => {
          setErrorMessage(""); // Redefine a mensagem de erro
          history.push("/home");
          window.location.reload(); // Atualiza a página
        }, 1000);  // Atraso de 1 segundo (1000 milissegundos) antes de atualizar a página
      } else {
        setErrorMessage("E-mail ou senha incorretos");
      }
    } catch (error) {
      setErrorMessage("Ocorreu um erro ao fazer o login");
      console.error(error);
    }
  };

  return (
    <div className="container-fluid">
      <h1 className="h1 mb-3 font-weight-normal"></h1>
      <div className="col-md-3 mx-auto">
        <form className="form-signin" onSubmit={handleSubmit(onSubmit)}>
          <div className="text-center mb-1">
            <img src="https://i.postimg.cc/TPSC6Y9h/logo-maior.png" width="350px" alt="Responsive image" />
            <h1 className="h3 mb-3 font-weight-normal"></h1>
          </div>

          <div className={errorMessage ? "alert alert-danger" : ""}>
            {errorMessage}
          </div>

          <div className="form-label-group">
            <input type="email" id="email" className="form-control" placeholder="Email do Cliente" required autoFocus {...register("email")} />
            <label htmlFor="email"> </label>
          </div>

          <div className="form-label-group">
            <input type="password" id="senha" className="form-control" placeholder="Senha" required {...register("senha")} />
            <label htmlFor="senha"></label>
          </div>

          <button className="btn btn-lg btn-success btn-block" type="submit">
            Entrar
          </button>

          <Link className="btn btn-lg btn-danger btn-block" to="">
            Esqueceu a Senha
          </Link>

          <Link className="btn btn-lg btn-primary btn-block" to="/cadastro">
            Criar Conta
          </Link>
        </form>
      </div>
    </div>
  );
};

export default Login;
