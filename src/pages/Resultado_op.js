import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import Conecta from "./config/Conecta";
import ItemLista from "./ItemListaResultado";
import Menu from "./Menu";
import "./css/Dashboard.css";
import NavBar_total from "./NavBar";
import jsPDF from "jspdf";
import "jspdf-autotable";

const ResultadoAtivos = () => {
  const [resultado_op, setResultado] = useState([]);
  const { register, handleSubmit, reset } = useForm();

  const obterLista = async () => {
    try {
      const lista = await Conecta.get("resultado_op");
      setResultado(lista.data);
    } catch (error) {
      alert(`Erro... Não foi possível obter os dados: ${error}`);
    }
  };

  useEffect(() => {
    obterLista();
  }, []);

  const handleGerarRelatorio = () => {
    const doc = new jsPDF();
    doc.setFont("Helvetica", "bold");
    doc.setFontSize(12);
  
    const tableData = resultado_op.map((resultado) => [
      resultado.nome,
      resultado.codigo,
      resultado.setor,
      `US$ ${resultado.resultado}`,
      resultado.diferenca_percentual,
      resultado.data_op
    ])
  
    doc.autoTable({
      head: [
        [
          "Nome da Empresa",
          "Código do Ativo",
          "Setor",
          "Resultado",
          "Percentual da OP",
          "Data Operação"
        ]
      ],
      body: tableData,
      startY: 40
    });
  
    doc.save("relatorio_resultados.pdf");
  };
  

  return (
    <div className="dash">
      <Menu />
      <div className="container">
        <nav className="navbar navbar-dark bg-primary">
          <a className="navbar-brand">Controle Invest</a>
          <form className="form-inline">
            <i className="mr-4">Resultado de Operações</i>
          </form>
        </nav>
        <table className="table table-striped mt-3">
          <thead>
            <tr>
              <th>Nome da Empresa</th>
              <th>Código do Ativo</th>
              <th>Setor</th>
              <th>Resultado</th>
              <th>Percentual da OP</th>
              <th>Data Operação</th>
            </tr>
          </thead>
          <tbody>
            {resultado_op.map((resultado, index) => (
              <ItemLista
                key={index}
                nome={resultado.nome}
                codigo={resultado.codigo}
                setor={resultado.setor}
                resultado={resultado.resultado}
                diferenca_percentual={resultado.diferenca_percentual}
                data_op={resultado.data_op}
              />
            ))}
          </tbody>
        </table>
        <div className="text-center">
          <button className="gerar-relatorio-btn" onClick={handleGerarRelatorio}>
            Gerar Relatório
          </button>
        </div>
      </div>
    </div>
  );
};

export default ResultadoAtivos;
