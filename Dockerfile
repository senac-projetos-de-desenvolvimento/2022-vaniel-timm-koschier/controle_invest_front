FROM node:latest

RUN npm install -g npm@8.13.1
COPY package.json package-lock.json ./
RUN npm install 
RUN npm install react-scripts@3.3.1 -g --silent

COPY . .
EXPOSE 3000

CMD [ "npm","start"]
