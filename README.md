# Controle Invest Front

## Baixar o projeto na sua maquina
```sh
git clone https://gitlab.com/senac-projetos-de-desenvolvimento/2022-vaniel-timm-koschier/controle_invest_front.git
```
## Baixar as dependencias 
### Entrar na pasta do projeto controle_invest_front e rodar o seguinte comando:
```sh
npm i
```
## Rodar o projeto 
### Dentro da pasta controle_invest_front rodar o seguinte comando:
### Lembrando que antes de iniciar o front deve ser inicializado a API.
```sh
npm start
```
